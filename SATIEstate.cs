﻿

using UnityEngine.UI;
using UnityEngine;
#if (UNITY_EDITOR) 
using UnityEditor;
#endif

//using UnityEditor;
using System;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Net;
using SATIEsimpleJSON;
using OSC.NET;



[ExecuteInEditMode]
public class SATIEstate : MonoBehaviour
{
    const string satieResponderMessAddr = "/satie/responder";
    const string pluginsQueryAddr =      "/satie/plugins";
    const string pluginsRxAddr =        "/plugins";
    const string pluginArgsQueryAddr = "/satie/pluginargs";
    const string pluginArgsRxAddr =   "/arguments";
        


    private bool osctXconnected = false;
    private bool oscRXconnected = false;
    public bool satieConnected = false;

    private bool _instanceState = false;


    public string address = "127.0.0.1";
    // defalut to localhost
    public int TXport = 18032;
    //MH faceShiftOSC default port
    static OSCTransmitter sender;


    public int RXport = 18042;
    //MH faceShiftOSC default port
    private OSCReceiver receiver;
    private Thread thread;

    private List<OSCMessage> processQueue = new List<OSCMessage>();

 
    // alert delegates when state changes
    public delegate void OnSatieStateRefresh();

    public static event OnSatieStateRefresh alertSubscribers;


    public static SATIEstate instance;


    public bool debug = false;
    private bool _watchDogState = false;

    // QUERRIED SATIE PLUGIN STATE
    static public string[] effectsList = { "empty" };
    static public string[] processesList = { "empty" };
    static public string[] spatializerList = { "empty" };
    static public string[] postprocessorList = { "empty" };
    static public string[] mapperList = { "empty" };
    static public string[] audiosourceList = { "empty" };
    static public List <satieNodeProperty> pluginArgs = new List <satieNodeProperty>();
    static public string pluginArgsPluginName = "";
    static public string pluginDescription = "";




    public SATIEstate()
    {       
 //       Debug.Log("SATIEstate() class initi called");
//        audiosourceList =  new string[10];

    }

    void Awake()
    {
        
        // Assert Singleton
        if (instance != null)
        {
            Debug.LogWarning(transform.name + "  " + GetType() + ".Awake(): Singleton instance of this class already exists, not creating another one for  " + transform.name);
            Destroy(this);
            return;
        }
        // else no instance exists yet, create the singleton
        instance = this;

    }

    void Update()
    {
        processOSC();   // this is not called in Editor mode
    }
   

    void Start()
    {

        if (debug)
        {
            if (!Application.isPlaying)
                Debug.Log(transform.name + "  " + GetType() + ".Start(): EDIT MODE");
            else if (Application.isPlaying)
                Debug.Log(transform.name + "  " + GetType() + ".Start(): PLAY MODE");
            else
                Debug.Log(transform.name + "  " + GetType() + ".Start(): UNRECOGNIZED MODE");
        }

        if (SATIEsetup.Instance == null)
        {
            Debug.LogError(transform.name + "  " + GetType() + ".Start() : SATIEsetup instance missing, aborting");
            Destroy(this);
        }

        address = SATIEsetup.getAddress();
        TXport = SATIEsetup.getTXport();
        RXport = SATIEsetup.getQueryPort();

        OSCconnect(true);

        if (!Application.isPlaying)
        {
            editModeWatchDog(true);   // only runs in edit mode
        }
    }

    // Edit Mode
    private int pollCount = 0;
    private int pollRate = 200;

    // only runs in editor mode
    void editModeWatchDog(bool state)
    {
        #if (UNITY_EDITOR) 
        if (Application.isPlaying)
            return;

        if (state)
        {
            if (_watchDogState)
                return;    // already watching 

            EditorApplication.update += editorConnect2satie;
            _watchDogState = true;
            pollCount = 0;
        } else
        {
            if (!_watchDogState)
                return;  // already disconnected
            if (_watchDogState)
                EditorApplication.update -= editorConnect2satie;
            _watchDogState = false;
        }
        #endif
    }

    // Edit Mode
    // code below attempts to establish contact with satie server
    void editorConnect2satie()
    {

        if (pollCount++ < pollRate)
            return;
        pollCount = 0;

        if (debug)
            Debug.Log(transform.name + "  " + GetType() + ".editorConnect2satie(): polling for OSC connection to satie *******************************************");

        if (!oscRXconnected || !osctXconnected)
        {
            Debug.LogWarning(transform.name + "  " + GetType() + ".editorConnect2satie():  off line:  OSC TX port status: " + osctXconnected + " OSC RX port status: " + oscRXconnected);
        } else if (instance != null)
        {
            if (debug)
                Debug.Log(transform.name + "  " + GetType() + ".connect2satie():  querying satie host");
            satieSubscribe();  // tell Satie to send any query relies to this IP and Port
            querySatie(SATIEsetup.satieQueryTypes.audiosource);    // ask for state
        } else if (debug)
            Debug.Log(transform.name + "  " + GetType() + ".editorConnect2satie(): no instance but still polling for OSC connection to satie *******************************************");
    }



    private float timeBetweenQueries = 1f;

      // code below attempts to establish contact with satie server
    IEnumerator playerConnect2satie()
    {
        Debug.Log(transform.name + "  " + GetType() + ".playerConnect2satie()");

        while ( satieConnected == false && _instanceState )
        {
            if (debug)
                Debug.Log(transform.name + "  " + GetType() + ".playerConnect2satie(): trying to establish OSC connection to satie *******************************************");
            
            if (!oscRXconnected || !osctXconnected)
            {
                Debug.LogWarning(transform.name + "  " + GetType() + ".playerConnect2satie():  off line:  OSC TX port status: " + osctXconnected + " OSC RX port status: " + oscRXconnected);
            } 
            else if (instance != null)
            {
                if (debug)
                    Debug.Log(transform.name + "  " + GetType() + ".playerConnect2satie():  subscribing to,  and querying satie host");
                satieSubscribe();  // tell Satie to send any query relies to this IP and Port
                querySatie(SATIEsetup.satieQueryTypes.audiosource);    // ask for state
            } 
            else if (debug)
                Debug.Log(transform.name + "  " + GetType() + ".playerConnect2satie(): no singleton instance but still polling for OSC connection to satie *******************************************");
            yield return new WaitForSeconds(timeBetweenQueries);
        }
    }

    void satieConnection(bool state)
    {
        if (!state)
            return;

 
        if (!satieConnected)
        {
            Debug.Log(transform.name + "  " + GetType() + ".satieConnection():  SATIE bidirectional <--------> connection established");
            satieConnected = true;
            editModeWatchDog(false); // connected now, so we can disable attempts to connect to satie
        }
    }

    // receiving satie state messages here
    void processOSC()
    {
        //processMessages has to be called on the main thread
        //so we used a shared proccessQueue full of OSC Messages
        lock (processQueue)
        {
            foreach (OSCMessage message in processQueue)
            {
                //BroadcastMessage("OSCMessageReceived", message, SendMessageOptions.DontRequireReceiver);
                if (debug) 
                {
                    Debug.Log("processOSC() RX mess: " + message.Address);
                    for (int i=0; i< message.Values.Count; i++) 
                        Debug.Log("value["+i+"] = "+message.Values[i]);
                 }

                switch (message.Address)
                {
 
                    case pluginsRxAddr:
                        if (message.Values.Count != 1)
                        {
                            Debug.Log("processOSC() RX mess: " + pluginsRxAddr + "  expects 1 string data item: ");
                            break;
                        }
                        OSCrxPluginDump((string)message.Values [0]);
                        satieConnection(true);  // we are receiving OSC on this port, so we are connected to satie
                        break;
                    case pluginArgsRxAddr:
                        if (message.Values.Count != 1)
                        {
                            Debug.Log("processOSC() RX mess: " + pluginArgsRxAddr + "  expects 1 string data item: ");
                            break;
                        }
                        OSCrxPluginArgDump((string)message.Values [0]);
                        satieConnection(true);  // we are receiving OSC on this port, so we are connected to satie
                        break;
                }
            }
            processQueue.Clear();
        }
    }


    void OnDestroy()
    {
        //editModeWatchDog(false);
        OSCconnect(false);

    }

    void connectToSatie()
    {
        satieConnected = false;
        if (!Application.isPlaying) 
            editModeWatchDog(true);
        else 
        {
            Debug.Log(transform.name + "  " + GetType() + "  StartCoroutine playerConnect2satie()");

            StartCoroutine(playerConnect2satie());
        }
    }

    void disconnectFromSatie()
    {
        editModeWatchDog(false);
    }

    void OnEnable()
    {
        _instanceState = true;
        #if (UNITY_EDITOR) 
        if (!Application.isPlaying) EditorApplication.update += processOSC;     
        #endif

        connectToSatie();
        if (debug)
            Debug.Log(transform.name + "  " + GetType() + ".OnEnable()");
    }

    void OnDisable()
    {
        _instanceState = false;

        #if (UNITY_EDITOR) 
        if (!Application.isPlaying) EditorApplication.update -= processOSC;   
        #endif

        disconnectFromSatie();
        if (debug)
            Debug.Log(transform.name + "  " + GetType() + ".OnDisable");
    }

    public void OnApplicationQuit()
    {
        if (debug)
            Debug.Log(transform.name + "  " + GetType() + ".OnApplicationQuit");
        OSCconnect(false);
    }

    void OSCconnect(bool state)
    {
        osctXconnected = oscRXconnected = false;
        //processQueue.Clear();

        if (state == true)
        {
            // Setup OSC TX
            try
            {
                Debug.Log(transform.name + " " + GetType() + "Awake() creating transmitter on port " + TXport);
                osctXconnected = true;
                sender = new OSCTransmitter(address, TXport);
                //thread = new Thread(new ThreadStart(listen));
                //thread.Start();
            } catch (Exception e)
            {
                Debug.LogError(transform.name + " " + GetType() + ".Awake()  failed to create transmitter on port " + TXport);
                Debug.LogWarning(e.Message);
            }
            //        string localHostName = Dns.GetHostName();
            //        IPHostEntry hostEntry = Dns.GetHostEntry(localHostName);
            //        foreach (IPAddress ipAddr in hostEntry.AddressList) {
            //            Debug.Log ("UnityOSCTransmitter: MY IP:" + ipAddr.ToString ());

            // Set up OSC RX
            try
            {
                Debug.Log(transform.name + " " + GetType() + "Awake() creating listener on port " + RXport);
                oscRXconnected = true;
                receiver = new OSCReceiver(RXport);
                thread = new Thread(new ThreadStart(listen));
                thread.Start();
            } catch (Exception e)
            {
                Debug.LogError(transform.name + " " + GetType() + ".Awake()  failed to create listener on port " + RXport);
                Debug.LogException(e);
            }
        } 
        else
        {
            if (receiver != null)
            {
                receiver.Close();
            }

            receiver = null;
            oscRXconnected = false;

            if (sender != null)
            {
                sender.Close();
            }
            sender = null;
            osctXconnected = false;
        }
    }

    static public void  initializePlugin(string pluginName, SATIEnode node)
    {
        if (pluginArgsPluginName == pluginName)
        {
            node.setProperties(pluginArgs);
            return;
        } else
        {
            Debug.LogError("SATIEstate.initializePlugin() plugin: " + pluginName + " parameters not available for node: " + node.nodeName);
            return;
        }
    }

    static public  void  querySatie(SATIEsetup.satieQueryTypes type)
    {
        string path;

        switch (type)
        {
            case  SATIEsetup.satieQueryTypes.audiosource: 
                path = pluginsQueryAddr;  // "/satie/query/plugins/audioSources";
                break;

            default:
                return;
        }
//        List<object> items = new List<object>();
//        SATIEsetup.OSCtx(path, items);
//        items.Clear();
        OSCMessage message = new OSCMessage(path);
        sendOSC(message);
    }

    static  public void querySatie(SATIEsetup.satieQueryTypes type, string pluginName)
    {
        string path;

        switch (type)
        {
            case  SATIEsetup.satieQueryTypes.audiosourceArgs: 
                path = pluginArgsQueryAddr; //  /satie/query/plugins/audioSourceParams";
                break;
            default:
                return;
        }

        OSCMessage message = new OSCMessage(path);
        message.Append(pluginName);
        sendOSC(message);
    }


    void  satieSubscribe()
    {
        string path = satieResponderMessAddr;
        string myIP = SATIEsetup.LocalIPaddress();  

        if (debug)
            Debug.Log(transform.name + " " + GetType() + ".querySatieSubscribe() : subscribing Satie4Unity to receive messages from Satie renderer my IP: " + myIP + " port: " + SATIEsetup.Instance.satieQueryPort);

        OSCMessage message = new OSCMessage(path);
        message.Append(myIP);
        message.Append(RXport);
        sendOSC(message);
    }

    public void OSCrxPluginDump(string pluginsJsonStr)
    {
        if (debug)
            Debug.Log(transform.name + " " + GetType() + ".OSCrxPluginDump() : RECEIVED: " + pluginsJsonStr);

        parsePluginDump(SATIEsetup.satieQueryTypes.audiosource, pluginsJsonStr);
    }

    public void OSCrxPluginArgDump(string pluginsJsonStr)
    {
        const string nullStr =  "\"" + "null" + "\"";

        if (debug)
            Debug.Log(transform.name + " " + GetType() + ".OSCrxPluginArgDump() : RECEIVED: " + pluginsJsonStr);

        if (pluginsJsonStr == nullStr)
        {
            Debug.LogWarning(transform.name + " " + GetType() + ".OSCrxPluginArgDump() : jsonStr is NULL: " + pluginsJsonStr);
            return;
        }
        parsePluginArgsDump(pluginsJsonStr);
   }


    // this parser is hard coded to the order of "synths" and "effects" keys.  It will probably break if any changes are made.  NEED TO PARSE BETTER
    public void parsePluginDump(SATIEsetup.satieQueryTypes pluginType, string jsonStr)
    {
        JSONNode jsonNode = JSON.Parse(jsonStr);
        List<String> parsedSources = new List<String>();
        List<String> parsedEffects = new List<String>();

        foreach (var node in jsonNode.Keys)
        {
            // Debug.Log("\t*******************  KEYS: " + node);
                        
        }

        for (int i = 0; i <= jsonNode.Count - 1; i++)
        {
            JSONObject job = (JSONObject)jsonNode [i];

//            if (i == 0)
//                Debug.Log("SYNTHS");
//            else if (i == 1)
//                Debug.Log("EFFECTS");

 
            foreach (var item in job.m_Dict)
            {
                if (i == 0)
                    parsedSources.Add(item.Key);
                else if ((i == 1))
                    parsedEffects.Add(item.Key);

                //                if (item.Key == "synths") parsedSources.Add(item.Key);
//                else if ((item.Key == "effects") ) parsedEffects.Add(item.Key);

                if (debug) Debug.Log("\t\t" + pluginType + " KEY " + item.Key + "    Value: " + item.Value);

//                switch (pluginType)
//                {
//                    case SATIEsetup.satieQueryTypes.audiosource:
//                        Debug.Log("\t\t"+ pluginType +" KEY " + item.Key+"    Value: "+ item.Value);
//                        parsedPlugins.Add(item.Key);
//                        break;
//
//                    default:
//                        //Debug.Log("\t\t"+ pluginType +" KEY " + item.Key+"    Value: "+ item.Value);
//                        break;
//                }
            }
        }

        int index = 0;
        audiosourceList = new string[parsedSources.Count];
        foreach (string plugName in parsedSources)
            audiosourceList [index++] = plugName;

        index = 0;
        effectsList = new string[parsedEffects.Count];
        foreach (string plugName in parsedEffects)
            effectsList [index++] = plugName;

     

        if (debug)
            Debug.Log(transform.name + " " + GetType() + "parsePluginDump()  parsed " + audiosourceList.Length + " source plugins and " + effectsList.Length + " effects plugins");
    }


    public void parsePluginArgsDump(string jsonStr)
    {

        if (debug)
            Debug.Log(transform.name + " " + GetType() + ".parsePluginArgsDump() : " + jsonStr);

        JSONNode jsonNode = JSON.Parse(jsonStr);

        string[] keyArray;

        pluginArgs.Clear();

        //Debug.Log  : "+jsonNode["synths"]);


        foreach (var nodeName in jsonNode.Keys)
        {
            if (debug) Debug.Log("\t parsing args for " + nodeName + " plugin");
            pluginArgsPluginName = nodeName;
        }

        for (int i = 0; i <= jsonNode.Count - 1; i++)
        {

            JSONObject job = (JSONObject)jsonNode [i];
            //string keyArray : TKey[] = yourDictionary.Keys().ToArray();
            //var jName = jsonNode[i].Value;

            foreach (var item in job.m_Dict)
            {
                if (item.Key == "description") pluginDescription = item.Value.ToString();

                    //Debug.Log("\t\t    KEY " + item.Key + "    Value: " + item.Value);

                if (item.Key == "arguments")
                {
                    JSONObject args = (JSONObject)item.Value;
                    foreach (var param in args.m_Dict)
                    {
                        if (param.Value.IsNumber || param.Value.IsString)
                        {
                            satieNodeProperty p = new satieNodeProperty();
                            p.name = param.Key;
                            p.value = param.Value;

                            pluginArgs.Add(p);
                            //Debug.Log("\t\t    KEY " + param.Key + "    Value: " + param.Value);
                        }
                    }
                }
            }
        }
        if (debug) Debug.Log("parsePluginArgsDump()  parsed out " + pluginArgs.Count + " key.pairs");
    }

    private void listen()
    {
        while (oscRXconnected)
        {
            try
            {
                OSCPacket packet = receiver.Receive();
                if (packet != null)
                {
                    lock (processQueue)
                    {

                        //Debug.Log( "adding  packets " + processQueue.Count );
                        if (packet.IsBundle())
                        {
                            ArrayList messages = packet.Values;
                            for (int i = 0; i < messages.Count; i++)
                            {
                                processQueue.Add((OSCMessage)messages [i]);
                            }
                        } else
                        {
                            processQueue.Add((OSCMessage)packet);
                        }
                    }
                } else
                    Console.WriteLine("null packet");
            } catch (Exception e)
            { 
                Debug.Log(e.Message);
                Console.WriteLine(e.Message); 
            }
        }
    }

    static  int sendOSC(OSCMessage mess)
    {
        int bytesSent = 0;
        OSCBundle objectBundle = new OSCBundle();
        objectBundle.Append(mess);
        bytesSent = sender.Send(objectBundle);
        return (bytesSent);
    }

}





