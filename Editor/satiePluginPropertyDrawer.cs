﻿using UnityEditor;
using UnityEngine;
using System.Text.RegularExpressions;

[CustomPropertyDrawer(typeof(satiePluginItem))]

public class SatiePluginItemPropertyDrawer : PropertyDrawer
{
    public int pluginIndex = 0;
    private string lastPluginName = "";


    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        SATIEnode srcNode = property.serializedObject.targetObject as SATIEnode;

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var nameRect = new Rect(position.x, position.y, 160, position.height);

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"), GUIContent.none);
 
        SerializedProperty sp = property.FindPropertyRelative("name");
        string pname = sp.stringValue;  

        if (pname == "")
        {
            switch (srcNode.satiePluginType)
            {
                case SATIEnode.satiePluginTypes.sound :
                    pname = "default";
                    break;
                case SATIEnode.satiePluginTypes.effect :
                    pname = "defaultFx";
                    break;
                case SATIEnode.satiePluginTypes.process :
                    pname = "defaultProcess";
                    break;
            }
        }

        sp.stringValue = (srcNode.satiePluginType == SATIEnode.satiePluginTypes.sound) ? Regex.Replace(pname, @"\s+", "") : pname;  // trim off white spaces for source nodes

        var pluginMenuRect = new Rect(position.x + 170, position.y, 200, position.height);
        var pluginDescriptionRect = new Rect(position.x + 170, position.y+18, 200, position.height);
        EditorGUI.LabelField(pluginDescriptionRect, new GUIContent(SATIEstate.pluginDescription));

        string newPluginName = "empty";

        switch (srcNode.satiePluginType)
        {
 
            case  SATIEnode.satiePluginTypes.sound: 
                pluginIndex = EditorGUI.Popup(pluginMenuRect, pluginIndex, SATIEstate.audiosourceList);
                newPluginName = (SATIEstate.audiosourceList.Length > pluginIndex) ? SATIEstate.audiosourceList [pluginIndex] : "empty";
                break;
            case  SATIEnode.satiePluginTypes.effect: 
                pluginIndex = EditorGUI.Popup(pluginMenuRect, pluginIndex, SATIEstate.effectsList);
                newPluginName = (SATIEstate.effectsList.Length > pluginIndex) ? SATIEstate.effectsList [pluginIndex] : "empty";
                break;
            case  SATIEnode.satiePluginTypes.process: 
                //pluginTypeIndex = EditorGUI.Popup(typesMenuRect, pluginTypeIndex, SATIEstate.processesList);
                //newPluginName = (SATIEstate.processesList.Length > pluginTypeIndex) ? SATIEstate.processesList [pluginTypeIndex] : "empty";
                break;

            default:
                return;
        }

        var buttonRect = new Rect(position.x - 44, position.y, 40, position.height);

        if (newPluginName != "empty")
        {
            if (newPluginName != lastPluginName)
            {
                lastPluginName = newPluginName;
                SATIEstate.querySatie(SATIEsetup.satieQueryTypes.audiosourceArgs, newPluginName);
            }
        }

        if (srcNode.satiePluginType == SATIEnode.satiePluginTypes.sound || srcNode.satiePluginType == SATIEnode.satiePluginTypes.effect)
        {
            if (newPluginName != "empty")
            {
 
                if (GUI.Button(buttonRect, "recall"))
                {   // the order below is sensitive.. to make the properties update properly on one click
                    sp = property.FindPropertyRelative("name");
                    SATIEstate.initializePlugin(newPluginName, srcNode);
                    property.serializedObject.Update();
                    sp.stringValue = newPluginName;
                }
                // add init button ?
            }
        }
        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }

}
